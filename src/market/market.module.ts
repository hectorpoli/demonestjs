import { BtcModule } from './btc/btc.module';
import { Module } from '@nestjs/common';


@Module({
    imports: [
        BtcModule,
    ],
    providers: [],
    controllers: [],
    exports: []
})
export class MarketModule {}