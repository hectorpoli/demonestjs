import { Controller, Get } from '@nestjs/common';
import { BtcService } from './btc.services';
import { AxiosResponse } from 'axios'
import { Observable } from 'rxjs';

@Controller('api/market/btc')
export class BtcController {

    constructor(
        private readonly btcService: BtcService
    ) {}
    
    @Get()
    getPrice(): Observable<AxiosResponse<any>> {
        return this.btcService.getPrice();
    }

}