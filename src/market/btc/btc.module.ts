import { BtcController } from './btc.controller';
import { Module, HttpModule } from '@nestjs/common';
import { BtcService } from './btc.services';


@Module({
  imports: [HttpModule],
  providers: [BtcService],
  controllers: [BtcController],
  exports: []
})
export class BtcModule {}