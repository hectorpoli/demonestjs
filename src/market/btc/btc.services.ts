import { Injectable, HttpService } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AxiosResponse } from 'axios'

@Injectable()
export class BtcService {
  constructor(
    private httpService: HttpService
  ) { }

  getPrice(): Observable<AxiosResponse<any[]>> {
    return this.httpService.get('https://blockchain.info/ticker')
    .pipe(
        map(resp => resp.data),
    );
  }
}