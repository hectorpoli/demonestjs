import { Module } from '@nestjs/common';
import { UsersModule } from './user/users.module';
import { GroupsModule } from './group/groups.module';

@Module({
    imports: [
        UsersModule,
        GroupsModule,
    ],
    providers: [],
    controllers: [],
    exports: []
})
export class AuthModule {}