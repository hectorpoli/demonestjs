import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Group } from './group.entity';
import { GroupInterface } from '../interface';

@Injectable()
export class GroupsService {
  constructor(
    @InjectRepository(Group)
    private groupsRepository: Repository<Group>,
  ) {}

  create(groupData: GroupInterface): Promise<Group> {
    const user = new Group();
    user.description = groupData.description;
    return this.groupsRepository.save(user);
  }
  
  findAll(): Promise<Group[]> {
    return this.groupsRepository.find();
  }

  findOne(id: string): Promise<Group> {
    return this.groupsRepository.findOne(id);
  }

  async remove(id: string): Promise<void> {
    await this.groupsRepository.delete(id);
  }
}