import { Controller, Delete, Get, Param, Post, Body } from '@nestjs/common';
import { Group } from './group.entity';
import { GroupsService } from './groups.service';
import { GroupInterface } from '../interface';

@Controller('api/groups')
export class GroupsController {

    constructor(
        private readonly groupsService: GroupsService
    ) {}

    @Post()
    create(@Body() userData: GroupInterface): Promise<Group> {
        return this.groupsService.create(userData);
    }
    
    @Get()
    findAll(): Promise<Group[]> {
        return this.groupsService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string): Promise<Group> {
        return this.groupsService.findOne(id);
    }

    @Delete(':id')
    remove(@Param('id') id: string): Promise<void> {
        return this.groupsService.remove(id);
    }
}