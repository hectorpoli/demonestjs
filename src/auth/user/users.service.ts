import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';
import { GroupsService } from '../group/groups.service';
import { UserInterface } from '../interface';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    private groupService: GroupsService,
  ) {}

  
  async create(userData: UserInterface): Promise<User> {
    const user = new User();
    user.firstName = userData.firstName;
    user.lastName = userData.lastName;
    
    const groupSelect = await this.groupService.findOne('1');
    console.log(groupSelect);
    user.groups = [];
    user.groups.push(groupSelect);
    return this.usersRepository.save(user);
  }
  

  findAll(): Promise<User[]> {
    return this.usersRepository.find({ relations: ["groups"] });
  }

  findOne(id: string): Promise<User> {
    return this.usersRepository.findOne(id);
  }

  async remove(id: string): Promise<void> {
    await this.usersRepository.delete(id);
  }
}