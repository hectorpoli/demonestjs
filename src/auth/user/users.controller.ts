import { Controller, Delete, Get, Param, Post, Body } from '@nestjs/common';
import { User } from './user.entity';
import { UsersService } from './users.service';
import { UserInterface } from '../interface';

@Controller('api/users')
export class UsersController {

    constructor(
        private readonly usersService: UsersService
    ) {}

    @Post()
    create(@Body() userData: UserInterface): Promise<User> {
        return this.usersService.create(userData);
    }
    
    @Get()
    findAll(): Promise<User[]> {
        return this.usersService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string): Promise<User> {
        return this.usersService.findOne(id);
    }

    @Delete(':id')
    remove(@Param('id') id: string): Promise<void> {
        return this.usersService.remove(id);
    }
}